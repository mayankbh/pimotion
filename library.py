import simplejson as json
import sys
import threading
import socket
import select

invalid_user_json = {"Result":"Invalid UID"}
invalid_password_json = {"Result":"Invalid Password"}
exception_json = {"Result":"Exception"}

SELECT_TIMEOUT=0.1

#Provides blocking access to gestures
#Remains blocked until response from user
#Returns UID, AID to calling function

class SyncGestureHandler:
    def add_user_to_access_list(self, new_uid, new_pass):
        if self.authenticated and new_uid not in self.permitted_users:
            self.permitted_users.append(new_uid)
            self.passwords[new_uid] = new_pass
            if self.debugging:
                print "Successfully added new user %s"% new_uid
            return True
        elif new_uid in self.permitted_users:
            if self.debugging:
                print "User %s already registered" % new_uid
            return False
        return True

    def remove_user_from_access_list(self, uid):
        if self.authenticated and uid in self.permitted_users:
            self.permitted_users.remove(uid)
            return True
        elif uid not in self.permitted_users:
            if self.debugging:
                print "User %s not registered, cannot remove" % uid
            return False

        return True

    def change_user_password(self, uid, new_pass):
        if self.remove_user_from_access_list(uid):
            return self.add_user_to_access_list(uid, new_pass)

        return False

    def registration_handler(self, conn, addr):
        
        received_data = ''
        if self.debugging:
            print "Registration Handler : Client " + str(addr) + " connected"
        
        while True:
            received = conn.recv(4096)
            #Append to string buffer
            if received == '':
                #Client closed
                break
            received_data += received
        
        received_data = received_data.strip()
        tempdata = received_data
        if received_data[0] == '\x26':
            if self.debugging:
                print "Updating tempdata"
            tempdata = received_data[1:]

        client_json = {}
        
        try:
            received_data = received_data.replace('\r\n', '').replace('\x00', '').replace('\x0f', '')
            tempdata = received_data[1:]
            if self.debugging:
                print tempdata
           # print "Attempting to parse %s\n" % repr(received_data)
            if self.debugging:
                print "Attempting to parse %s\n" % repr(tempdata)
#            client_json = json.loads(received_data)
            client_json = json.loads(tempdata)
            if self.debugging:
                print "Parsed JSON"

            uid = client_json["UID"]
            if self.authenticated:
                #Look for passkey in payload
                if uid not in self.permitted_users:
                    if self.debugging:
                        print "User not in access list"
                    conn.sendall(json.dumps(invalid_user_json))
                    conn.close()
                    return
                
                if "Password" not in client_json or self.passwords[uid] != client_json["Password"]:
                    if self.debugging:
                        print "Mismatched password"
                    conn.sendall(json.dumps(invalid_password_json))
                    conn.close()
                    return
        except ValueError as e:
            if self.debugging:
                print "Failed to parse client JSON : %s" % str(e)
            conn.sendall(json.dumps(exception_json))
            conn.close()
            return
        except Exception, err:
            if self.debugging:
                print "Exception occurred"
                print err
            conn.sendall(json.dumps(exception_json))
            conn.close()
            return

        #If control reaches here, authentication went okay or wasn't needed at all
        if self.debugging:
            print "User %s connected" % client_json["UID"]
            print "Sending response:%s" % repr(self.registration_json)
        
        conn.sendall(self.registration_json)
        conn.close()
        return
    
    def registration_thread(self):
        if self.debugging:
            print "Registration master thread spawned"
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.bind(('', self.registration_port))
        except socket.error as msg:
            if self.debugging:
                print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1] + ' (Registration thread)'
            sys.exit()
        
        s.listen(5)
        read_list = [s]
        while True:
#            if self.debugging:
#                print "Calling select()"
            readable, writable, errored = select.select(read_list, [], [], SELECT_TIMEOUT)
#            if self.debugging:
#                print "Returned from select()"
            for so in readable:
                conn, addr = so.accept()
                threading.Thread(target=self.registration_handler, args=(conn, addr)).start()

#            if self.debugging:
#                print "Exited so loop"

            #Check quit flag
            if self.finished:
                if self.debugging:
                    print "Finished flag is true"
                break
        s.close()
        
    def construct_registration_json(self):
        data = {}
        function_array = []
        for i in range (0, len(self.function_names)):
            function_map = {}
            function_map["ActionName"] = self.function_names[i]
            function_map["AID"] = i
            function_array.append(function_map)
            
        data["Port"] = self.gesture_port
        data["ActionList"] = function_array
        data["Result"] = "OK"

        return json.dumps(data)

    def cleanup(self):
        self.finished = True
        #Child threads will clean themselves up

    def __init__(self, registration_port, function_name_list, authentication_needed, debugging=False):
        self.debugging = debugging       
        self.passwords = {}
        self.finished = False

        #Set registration port
        self.registration_port = registration_port
        self.gesture_port = registration_port + 1
        self.authenticated = authentication_needed
        
        #Set function names
        self.function_names = function_name_list 
        self.registration_json = self.construct_registration_json()
       
        #Set access list
        self.permitted_users = []

        threading.Thread(target=self.registration_thread).start()

        self.gesture_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.gesture_socket.bind(('', self.gesture_port))
        except socket.error as msg:
            if self.debugging:
                print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1] + ' (Registration thread)'
            return None
        self.gesture_socket.listen(5)

    def assert_type( self, variable, variable_name, expected_type):
        if not(isinstance(variable, expected_type)):
            if self.debugging:
                print "Type assertion failed for " + variable_name + ", got " + str(type(variable)) + ", expected " + str(expected_type)
            return False
        
        return True

    def get_next_action(self):

        received_data = ''
        conn, addr = self.gesture_socket.accept()
        while True:
            received = conn.recv(4096)
            #Append to string buffer
            if received == '':
                #Client closed
                break
            received_data += received
        
        received_data = received_data.strip()
        client_json = {}
        
        action_id = -1

        try:
            received_data = received_data.replace('\r\n', '').replace('\x00', '').replace('\x0f', '')
            tempdata = received_data[1:]

            #print "Attempting to parse %s\n" % repr(received_data)
            if self.debugging:
                print "Attempting to parse %s\n" % repr(tempdata)
            #client_json = json.loads(received_data)
            client_json = json.loads(tempdata)
            uid = client_json["UID"]
            action_id = client_json["AID"]
        except ValueError as e:
            if self.debugging:
                print "Failed to parse client JSON : %s" % str(e)
            conn.close()
            return None, None
        except Exception, err:
            if self.debugging:
                print "Exception occurred"
                print err
            conn.close()
            return None, None
        
        return (int(action_id), uid)

#Usage : Register callbacks and have them asynchronously called
#No thread safety as of now
class AsyncGestureHandler:
    
    def add_user_to_access_list(self, new_uid, new_pass):
        if self.authenticated and new_uid not in self.permitted_users:
            self.permitted_users.append(new_uid)
            self.passwords[new_uid] = new_pass
            if self.debugging:
                print "Successfully added new user %s"% new_uid
            return True
        elif new_uid in self.permitted_users:
            if self.debugging:
                print "User %s already registered" % new_uid
            return False
        return True

    def remove_user_from_access_list(self, uid):
        if self.authenticated and uid in self.permitted_users:
            self.permitted_users.remove(uid)
            return True
        elif uid not in self.permitted_users:
            if self.debugging:
                print "User %s not registered, cannot remove" % uid
            return False

        return True

    def change_user_password(self, uid, new_pass):
        if self.remove_user_from_access_list(uid):
            return self.add_user_to_access_list(uid, new_pass)

        return False

    def registration_handler(self, conn, addr):
        
        received_data = ''
    
        if self.debubbing:
            print "Registration Handler : Client " + str(addr) + " connected"
        
        while True:
            received = conn.recv(4096)
            #Append to string buffer
            if received == '':
                #Client closed
                break
            received_data += received
        
        received_data = received_data.strip()
        tempdata = received_data
        if received_data[0] == '\x26':
            if self.debugging:
                print "Updating tempdata"
            tempdata = received_data[1:]

        client_json = {}
        
        try:
            received_data = received_data.replace('\r\n', '').replace('\x00', '').replace('\x0f', '')
            tempdata = received_data[1:]
            print tempdata
           # print "Attempting to parse %s\n" % repr(received_data)
            if self.debugging:
                print "Attempting to parse %s\n" % repr(tempdata)
#            client_json = json.loads(received_data)
            client_json = json.loads(tempdata)
            if self.debugging:
                print "Parsed JSON"

            uid = client_json["UID"]
            if self.authenticated:
                #Look for passkey in payload
                if uid not in self.permitted_users:
                    if self.debugging:
                        print "User not in access list"
                    conn.sendall(json.dumps(invalid_user_json))
                    conn.close()
                    return
                
                if "Password" not in client_json or self.passwords[uid] != client_json["Password"]:
                    if self.debugging:
                        print "Mismatched password"
                    conn.sendall(json.dumps(invalid_password_json))
                    conn.close()
                    return
        except ValueError as e:
            if self.debugging:
                print "Failed to parse client JSON : %s" % str(e)
            conn.sendall(json.dumps(exception_json))
            conn.close()
            return
        except Exception, err:
            if self.debugging:
                print "Exception occurred"
                print err
            conn.sendall(json.dumps(exception_json))
            conn.close()
            return

        #If control reaches here, authentication went okay or wasn't needed at all
        if self.debugging:
            print "User %s connected" % client_json["UID"]
            print "Sending response:%s" % repr(self.registration_json)
        
        conn.sendall(self.registration_json)
        conn.close()
        return
    
    def registration_thread(self):
        if self.debugging:
            print "Registration master thread spawned"
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.bind(('', self.registration_port))
        except socket.error as msg:
            if self.debugging:
                print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1] + ' (Registration thread)'
            sys.exit()
        
        s.listen(5)
        read_list = [s]
        while True:
            readable, writable, errored = select.select(read_list, [], [], SELECT_TIMEOUT)
            for so in readable:
                conn, addr = so.accept()
                threading.Thread(target=self.registration_handler, args=(conn, addr)).start()

            #Check quit flag
            if self.finished:
                if self.debugging:
                    print "Finished flag is true"
                break
        
        s.close()

    def gesture_thread(self):
        if self.debugging:
            print "Gesture master thread spawned"
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.bind(('', self.gesture_port))
        except socket.error as msg:
            if self.debugging:
                print 'Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1] + ' (Gesture thread)'
            sys.exit()    
        
        s.listen(5)
        read_list = [s]
        while True:
            readable, writable, errored = select.select(read_list, [], [], SELECT_TIMEOUT)
            for so in readable:
                conn, addr = so.accept()
                threading.Thread(target=self.registration_handler, args=(conn, addr)).start()

            #Check quit flag
            if self.finished:
                if self.debugging:
                    print "Finished flag is true"
                break
            #conn, addr = s.accept()
            #threading.Thread(target=self.gesture_handler, args = (conn, addr)).start()
        s.close()
    
    def gesture_handler(self, conn, addr):
        received_data = ''
    
        if self.debugging:
            print "Gesture Handler : Client " + str(addr) + " connected"

        
        while True:
            received = conn.recv(4096)
            #Append to string buffer
            if received == '':
                #Client closed
                break
            received_data += received
            
        conn.close()
        
        client_json = {}
        
        try:
            received_data = received_data.strip().replace('\x00', '').replace('\x0f', '').replace('\r\n', '')
            if self.debugging:
                print "Attempting to parse:%s" % repr(received_data)
            client_json = json.loads(received_data)
        except:
            if self.debugging:
                print "Failed to parse client JSON"
        
        action_id = client_json["AID"]
        if self.debugging:    
            print "Received action %d" % action_id
        
        #Call the callback function
        self.functions[action_id]()
        
        
        return
    
    def construct_registration_json(self):
        data = {}
        function_array = []
        for i in range (0, len(self.function_names)):
            function_map = {}
            function_map["ActionName"] = self.function_names[i]
            function_map["AID"] = i
            function_array.append(function_map)
            
        data["Port"] = self.gesture_port
        data["ActionList"] = function_array
        data["Result"] = "OK"

        return json.dumps(data)
    
    def cleanup(self):
        self.finished = True
        #Child threads will clean themselves up

    def __init__(self, registration_port, function_list, function_name_list, authentication_needed, debugging=False):
        
        self.debugging = debugging
        self.finished = False


        if self.register_gesture_callbacks(registration_port, function_list, function_name_list) != 0:
            if self.debugging:
                print "Failed to initialize GestureHandler"
            return None
        
        self.passwords = {}

        #Set registration port
        self.registration_port = registration_port
        self.gesture_port = registration_port + 1
        self.authenticated = authentication_needed
        
        #Set function lists
        self.functions = function_list
        self.function_names = function_name_list 
        self.registration_json = self.construct_registration_json()
       
        #Set access list
        self.permitted_users = []

        threading.Thread(target=self.registration_thread).start()
        threading.Thread(target=self.gesture_thread).start()

    def assert_type( self, variable, variable_name, expected_type):
        if not(isinstance(variable, expected_type)):
            if self.debugging:
               print "Type assertion failed for " + variable_name + ", got " + str(type(variable)) + ", expected " + str(expected_type)
            return False
        
        return True

    def register_gesture_callbacks(self,  registration_port,  function_list,  function_name_list):
        if self.debugging:    
            print "Registration port = " + str(registration_port)
            print "Functions = " + str(function_list)
            print "Function names = " + str(function_name_list)
        
        self.assert_type(registration_port, "registration_port", int) 
        self.assert_type(function_list, "function_list", list)
        self.assert_type(function_name_list, "function_name_list", list)
        
        if registration_port < 0 or registration_port > 65535:
            if self.debugging:
                print "Invalid port number %d. Expected 0-65535" % registration_port
            return -1
    
        if len(function_list) != len(function_name_list):
            if self.debugging:
                print "Unequal lengths for function list and function name list (Functions : %d, Function names : %d" % (len(function_list), len(function_name_list))
            return -1
    
    
        #Success, return 0
        return 0
