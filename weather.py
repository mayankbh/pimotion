#!/usr/bin/python2

owm_api_key='01b3f6883e7a82b8e8a170102b396af3'

import pyowm
import json
import library
import praw

from googlefinance import getQuotes
from hackernews import HackerNews

owm = pyowm.OWM(owm_api_key)

WEATHER = 0
STOCKS = 1
SOCIAL_NEWS = 2
REDDIT = 3
QUIT = 4

functions = ['Weather', 'Stocks', 'Social News', 'Reddit', 'Quit']
handler = library.SyncGestureHandler(9030, functions, True, True)
handler.add_user_to_access_list('mayank', 'password')
hn =  HackerNews()
r = praw.Reddit(user_agent='PiMotion')

def weather():
    print "Getting weather for Goa, India"
    fc = owm.daily_forecast('Goa, in').get_forecast()
    for weather in fc:
        print weather.get_reference_time()
        print weather.get_status()
        print weather.get_temperature()

def stocks():
    print "Getting quotes for Google"
    try:
        print json.dumps(getQuotes('GOOG'), indent = 2)
    except:
        print "An error occurred"

def news():
    print "Fetching top 10 stories from Hacker News"
    for story_id in hn.top_stories(limit=10):
        print hn.get_item(story_id)

def reddit():
    print "Fetching top 10 posts from r/technology"
    print r.get_subreddit('technology').get_hot(limit=10)    

while True:
    print "Ready for next action"
    aid, uid = handler.get_next_action()
    try:
        if aid == WEATHER:
            weather()
        elif aid == STOCKS:
            stocks()
        elif aid == SOCIAL_NEWS:
            news()
        elif aid == REDDIT:
            reddit()
        elif aid == QUIT:
            break
    except Exception, err:
        print err

print "Quitting"
handler.cleanup()
