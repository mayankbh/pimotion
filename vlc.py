#!/usr/bin/python2

import sys
import library
import re
from subprocess import Popen, PIPE, STDOUT




def write_to_pipe(string):
    p.stdin.write(string)

def play():
    write_to_pipe('play\n')
def pause():
    write_to_pipe('pause\n')

def _next():
    write_to_pipe('next\n')

def prev():
    write_to_pipe('prev\n')

def vol_up():
    write_to_pipe('volup 10\n')   
    p.stdout.readline()

def vol_down():
    write_to_pipe('voldown 10\n')   
    p.stdout.readline()

def seek_forward_1():
    write_to_pipe('get_time\n')
    current_time = int(p.stdout.readline().replace('\n', '').replace('>', '').replace('\r', '').replace(r"\(.*\)",""))
    print "Python returned %s" % current_time
    write_to_pipe('seek ' + str(current_time + 60) + '\n')

def seek_backward_1():
    write_to_pipe('get_time\n')
    current_time = int(p.stdout.readline().replace('\n', '').replace('>', '').replace('\r', '').replace(r"\(.*\)",""))
    print "Python returned %s" % current_time
    write_to_pipe('seek ' + str(current_time -60) + '\n')


if __name__ == '__main__':
    p = Popen(['vlc', '-I', 'rc']+ sys.argv[1:], stdout=PIPE, stdin=PIPE, stderr=PIPE)
    p.stdout.readline()
    p.stdout.readline()
    functions = [play,pause, _next, prev, vol_up, vol_down, seek_forward_1, seek_backward_1]
    function_names = ['Play', 'Pause', 'Next', 'Prev', 'Volup', 'Voldown', '+1m', '-1m']
    handler = library.GestureHandler(9092, functions, function_names)
   
