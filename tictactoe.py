#!/usr/bin/python2

import library
import curses
import random

xxx = []
ooo = []

def reset_board():
    xxx = []
    ooo = []

def introduction(screen):
    screen.addstr(5, 30, "Welcome to Tic Tac Toe")
    screen.addstr(7, 20, "First two people will get to play")
    screen.addstr(9, 20, "Send any random action to join the game")
    screen.refresh()


UP = 0
DOWN = 1
LEFT = 2
RIGHT = 3
PLACE = 4
CONFIRM = 5
GIVE_UP = 6
RANDOM_PLACEMENT = 7

BASE_Y = 20
BASE_X = 20
SCALE_X = 3
SCALE_Y = 3


def draw_board(screen):
    empty = range(9)
    for x in xxx:
        empty.remove(x)

    for x in ooo:
        empty.remove(x)


    for x in xxx:
        screen.addstr(BASE_Y + int(x/3) * SCALE_Y, BASE_X + int(x % 3) * SCALE_X, "X")
    for x in ooo:
        screen.addstr(BASE_Y + int(x/3) * SCALE_Y, BASE_X + int(x % 3) * SCALE_X, "O")
    for x in empty:
        screen.addstr(BASE_Y + int(x/3) * SCALE_Y, BASE_X + int(x % 3) * SCALE_X, "-")
    screen.refresh()
        

def play_next_turn(screen, handler, player, marker):
    pass

def victory_condition(current_marker):
    if current_marker == 'X':
        list_to_check = xxx
    elif current_marker == 'O':
        list_to_check = ooo

    l = list_to_check

    #Row 1
    if 0 in l and 1 in l and 2 in l:
        return True
    
    #Row 2
    if 3 in l and 4 in l and 5 in l:
        return True

    #Row 3
    if 6 in l and 7 in l and 8 in l:
        return True

    #Column 1
    if 0 in l and 3 in l and 6 in l:
        return True

    #Column 2
    if 1 in l and 4 in l and 7 in l:
        return True

    #Column 3
    if 2 in l and 5 in l and 8 in l:
        return True

    #Diagonal 1
    if 0 in l and 4 in l and 8 in l:
        return True

    #Diagonal 2
    if 2 in l and 4 in l and 6 in l:
        return True

    return False

def movetoyx(screen, loc):
    y = int(loc/3)
    x = int(loc%3)
    screen.move(BASE_Y + y * SCALE_Y, BASE_X + x * SCALE_X)

def print_feedback(screen, string):
    screen.addstr(16, 20, '                                          ')
    screen.addstr(16, 20, string)
    screen.refresh()

def play_next_turn(screen, handler, current_player, current_marker):
    curses.curs_set(1)  #Set to underline mode
    
    selected_location = 4
    if current_marker == 'X':
        list_to_edit = xxx
    else:
        list_to_edit = ooo

    while(True):
        y = int(selected_location/3)
        x = int(selected_location%3)
        
        movetoyx(screen, selected_location)
        screen.refresh()
        action, player = handler.get_next_action()

        if player != current_player:
            print_feedback(screen, 'Wrong player tried to move')
            continue

        if action == LEFT and x > 0:
            x = x - 1
            selected_location = y * 3 + x
        elif action == RIGHT and x < 2:
            x = x + 1
            selected_location = y * 3 + x
        elif action == UP and y > 0:
            y = y - 1
            selected_location = y * 3 + x
        elif action == DOWN and y < 2:
            y = y + 1
            selected_location = y * 3 + x
        elif action == PLACE:
            if selected_location in xxx or selected_location in ooo:
                print_feedback(screen, "Location already occupied")
                continue
            print_feedback(screen, 'Confirm placement')
            action, player = handler.get_next_action()
            if player != current_player or action != CONFIRM:
                print_feedback(screen, 'Failed to confirm placement')
            else:
                list_to_edit.append(selected_location)
                curses.curs_set(0)  #Set to underline mode
                return PLACE

        elif action == CONFIRM:
            print_feedback(screen, 'Nothing to confirm')

        elif action == GIVE_UP:
            print_feedback(screen, 'Really give up? (Confirm please)')
            action, player = handler.get_next_action()
            if player != current_player or action != CONFIRM:
                print_feedback(screen, 'Failed to confirm give up')
            else:
                curses.curs_set(0)  #Set to underline mode
                return GIVE_UP

        elif action==RANDOM_PLACEMENT:
            print_feedback(screen, 'Confirm random placement')
            action, player = handler.get_next_action()
            if player != current_player or action != CONFIRM:
                print_feedback(screen, 'Failed to confirm random placement')
            else:
                while True:
                    newpos = random.randint(0, 8)
                    if newpos not in xxx and newpos not in ooo:
                        list_to_edit.append(newpos)
                        selected_location = newpos
                        break
                curses.curs_set(0)  #Set to underline mode
                return PLACE    

empty = []

def main():
    empty = range(9)
    random.seed()
    screen = curses.initscr()
    curses.curs_set(0)

    introduction(screen)
    reset_board()
    actions = ['Up', 'Down', 'Left', 'Right', 'Place', 'Confirm', 'Give up', 'Random Placement']
    handler = library.SyncGestureHandler(9050, actions, False, False)
    player1 = ""
    player2 = ""

    screen.addstr(9, 20, "Make a dummy action to register yourself, player1")
    action_id, player1 = handler.get_next_action()
    screen.addstr(10, 20, "Welcome, " + player1)
    screen.refresh()

    screen.addstr(11, 20, "Make a dummy action to register yourself, player2")
    screen.refresh()
    while True:
        action_id, player2 = handler.get_next_action()
        if player1 != player2:
            break;
    screen.addstr(12, 20, "Welcome, " + player2)

    screen.addstr(14, 30, "The game begins!")
    screen.refresh()

    draw_board(screen)
    
    current_player = player1
    current_marker = 'X'
    victor = None
    while victor == None and len(xxx) != 5:
        draw_board(screen)
        ret = play_next_turn(screen, handler, current_player, current_marker)
        if victory_condition(current_marker):
            victor = current_player
        if current_player == player1:
            current_marker = 'O'
            current_player = player2
        elif current_player == player2:
            current_marker = 'X'
            current_player = player1
        
        if ret == GIVE_UP:
            #Current player is now victor
            victor = current_player

    draw_board(screen)

    if victor !=  None:
        screen.addstr(40, 20, "Winner is " + victor)
    else:
        screen.addstr(40, 20, "You're both losers! Or winners!")

    screen.addstr(42, 20, "Player 1, give up to exit")
    screen.refresh()

    while True:
        aid, uid = handler.get_next_action()
        if uid == player1 and aid == GIVE_UP:
            break;

    curses.endwin()

    handler.cleanup()
    return

if __name__ == '__main__':
    main()
