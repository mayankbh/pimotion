#!/bin/python2

import library
import subprocess

def one():
    print "One performed"
    subprocess.call(["clementine"])
    return

def two():
    print "Two performed"
    subprocess.call(["vlc"])
    return

def three():
    print "Three performed"
    subprocess.call(["chromium"])
    return


def four():
    print "Four performed"
    subprocess.call(["firefox"])
    return


def five():
    print "Five performed"
    subprocess.call(["eiskaltdcpp-qt"])
    return


def six():
    print "Six performed"
    subprocess.call(["libreoffice"])
    return


def seven():
    print "Seven performed"
    subprocess.call(["wine", "/home/mayankbh/Diablo II/PlugY.exe"])
    return


def eight():
    print "Eight performed"
    subprocess.call(["echo", "BITCHEZ CRAY"])
    return

if __name__ == '__main__':
    functions = [one, two, three, four, five, six, seven, eight]
    function_names = ['Clementine', 'VLC', 'chromium', 'Firefox', 'DC', 'libre', 'Diablo', 'Echo test']
    print str(type(functions))
    print str(type(function_names))
    
    handler = library.GestureHandler(9090, functions, function_names)
    
    while True:
        pass
